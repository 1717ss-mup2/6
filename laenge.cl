#!/usr/bin/env clisp

;author Leonard Krause

(defun len (l)
  (if (eq (cdr l) nil)
    1
    (+ 1 (len (cdr l)))
  )
)

(defun sumlen (l)
  (if (eq (cdr l) nil)
    (len (car l))
    (+ (len (car l)) (sumlen (cdr l)))
  )
)

(defun mitlaenge (l)
  (/ (sumlen l) (len l))
)

(print "mitlaenge '((1 2 3) (A B) (S ta X 2)) expected: 3")
(print (mitlaenge '((1 2 3) (A B) (S ta X 2))))
(print "mitlaenge '((1 2 3 4 5 6 7) (1 2 3 4) (1 2)) expected: 4")
(print (mitlaenge '((1 2 3 4 5 6) (1 2 3 4) (1 2))))
(print "mitlaenge '((1 2 3 4 5 6 7) (1 2 3 4) (1 2)) expected: 15/4")
(print (mitlaenge '((1 2 3 4 5 6 7) (1 2 3 4) (1 2) (1 2))))


;Eingabe von Liste
(format t "~%~%Eingabe der Liste: ")
(setq Liste (read))
(print (mitlaenge Liste))
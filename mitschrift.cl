#!/usr/bin/env clisp

;(dolist (eineVariable liste))


(t) ; wahr

(defun printliste (liste)
	(cond
		(
			(not (eq liste nil))	;Bedingung
			(print (car liste))		;wahr
			(printliste (cdr liste));wahr
		)
		(t)	;leerer dummy, (t) wird einfach als wahr ausgewertet
	)
)

;Bedingunen verknuepfen
(and (not (eq liste nil)) (Bedingung2))


; liste ab n-ten element zurückgeben
(nthcdr n '(1 2 3 4))

; n-tes element aus liste zurückgeben
(car (nthcdr n '(1 2 3 4)))


;listen zusammenfügen
(cons wert (nthcdr inde liste)) ;speichert die neue liste nicht in der ursprungsliste
(setq dummy (cons wert (nthcdr index liste)))
(append liste1 liste2)

;find
(defun fineInListe (element liste)
	(if (find element liste)
		(format t "Element ~d gefunden ~%" element)
		(format t "Element ~d nicht gefunden ~%" element)
	)
)

;delete
(loesche '(1 2 3 4) 3)

(defun loesche(lsite element)
	(print liste) ;liste => 1 2 3 4
	(delete element liste) ;liste => 1 2 ~3~ 4
	(print liste) ;> liste => 1 2 4
)
#!/usr/bin/env clisp

(print "pls read the comments")

(defun len (l)
  (if (eq (cdr l) nil)
    1
    (+ 1 (len (cdr l)))
  )
)

(defun sumlen (l)
  (if (eq (cdr l) nil)
    (len (car l))
    (+ (len (car l)) (sumlen (cdr l)))
  )
)

(print (sumlen '('(1 2 3 4 5) '(1 2 3 4) '(1 2 3)))) ;expected: 12 actual: 6


(print (car '('(1 2 3)))) ;expected '(1 2 3) actual: '(1 2 3)
(print (len (car '('(1 2 3))))) ;expected: 3 actual: 2
(print (len '(1 2 3))) ;expected 3: actual 3

;why is lisp so strange???


;everything inside the quote function wont be evaluated.... therefor quotes in quotes have some strange effect :/
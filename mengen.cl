#!/usr/bin/env clisp

;author Leonard Krause

(defun vereinigung (A B)
  (set 'C A)
  (dolist (e B)
    (if(not(member e C))
      (push e C)
    )
  )
  C
)

(defun durchschnitt (A B)
  (set 'C nil)
  (dolist (e B)
    (if(member e A)
      (push e C)
    )
  )  
  C
)

(defun differenz (A B)
  (set 'C nil)
  (dolist (e A)
    (if(not(member e B))
      (push e C)
    )
  )  
  C
)

(defun potenz (A)
	(set 'C nil)
	(dolist (x A)
		(dolist (y A)
			(set 'temp nil) (push x temp) (push y temp)
			(push temp C)
		)
	)
	C
)
;Beispiel 1
(format t "~%~%Beispiel 1")
(format t "~%A = (1 2 3 4)")
(setq A '(1 2 3 4))
(format t "~%B = (1 2 3 4)~%")
(setq B '(1 2))
(format t "A vereinigt B = ")
(print (vereinigung A B))
(format t "~%A geschnitten B = ")
(print (durchschnitt A B))
(format t "~%A \ B = ")
(print (differenz A B))
(format t "~%P(A) = ")
(print (potenz A))
(format t "~%P(B) = ")
(print (potenz B))

;Beispiel 2
(format t "~%~%Beispiel 2")
(format t "~%A = (Hallo Welt)")
(setq A '(Hallo Welt))
(format t "~%B = (Hurra die Welt geht unter)~%")
(setq B '(Hurra die Welt geht unter))
(format t "A vereinigt B = ")
(print (vereinigung A B))
(format t "~%A geschnitten B = ")
(print (durchschnitt A B))
(format t "~%A \ B = ")
(print (differenz A B))
(format t "~%P(A) = ")
(print (potenz A))
(format t "~%P(B) = ")
(print (potenz B))

;Beispiel 3
(format t "~%~%Beispiel 3")
(format t "~%A = (a b c)")
(setq A '(a b c))
(format t "~%B = (a b)~%")
(setq B '(a b))
(format t "A vereinigt B = ")
(print (vereinigung A B))
(format t "~%A geschnitten B = ")
(print (durchschnitt A B))
(format t "~%A \\ B = ")
(print (differenz A B))
(format t "~%P(A) = ")
(print (potenz A))
(format t "~%P(B) = ")
(print (potenz B))


;Eingabe

(format t "~%~%Eingabe von A und B~%")

;Eingabe von A
(format t "A=")
(setq A (read))
;Eingabe von B
(format t "B=")
(setq B (read))

(format t "A vereinigt B = ")
(print (vereinigung A B))

(format t "~%A geschnitten B = ")
(print (durchschnitt A B))

(format t "~%A ohne B = ")
(print (differenz A B))

(format t "~%P(A) = ")
(print (potenz A))

(format t "~%P(B) = ")
(print (potenz B))
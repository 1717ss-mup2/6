#!/usr/bin/env clisp


(if (member '(1 1) '((1 1) (1 2) (2 2)))
	(print "true")
	(print "false")
)

(set a '(1 1))
(if (find a '((1 1) (1 2) (2 2)))
	(print "true")
	(print "false")
)


;why are these always true? (1 1) is in ((1 1) (1 2) (2 2))
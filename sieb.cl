#!/usr/bin/env clisp

;author Leonard Krause

(defun createSieb (n liste)
	(if (> n 1)
		(progn
			(push n liste)
			(createSieb (- n 1) liste)
			)
		liste
		)
	)

(defun loeschen (e liste)
	(if (eq liste nil)
		nil
		(progn 
			(if (eq (mod (car liste) e) 0)
				(loeschen e (cdr liste))
				(cons (car liste) (loeschen e (cdr liste)))
				)
			)
		)
	)

(defun siebenRek (sieb result n)
	(push (car sieb) result)
	(if (eq (cdr sieb) nil) ;nicht mehr wirklich notwendig...
		result ; ende des siebes -> result ist das ergebnis
		(progn
			(setq sieb (loeschen (car sieb) (cdr sieb)))
			(if (< (* (car sieb) (car sieb)) n) ;falls false stehe im sieb nur noch primzahlen -> reuslt + sieb ausgeben
				(siebenRek sieb result n)
				(append result sieb)
				)
			)
		)
	)

(defun sieben (n)
	(siebenRek (createSieb n nil) nil n)
	)

(format t "~%~%n=7 ")
(print (sieben 7))
(format t "~%~%n=10 ")
(print (sieben 10))
(format t "~%~%n=100 ")
(print (sieben 100))
(format t "~%~%n=1000 ")
(print (sieben 1000))

;Eingabe n
(format t "~%~%Eingabe von n ~% n=")
(setq n (read))
(print (sieben n))

